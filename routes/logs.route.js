const { Router } = require("express");

module.exports = app => {
    const logs = require("../controllers/logs");
    var router = require("express").Router();

    //get logs user
    router.get('/user/:id',logs.logsuser)
    
    app.use('/api/logs', router);
}