module.exports = app => {
  const users = require("../controllers/user.controller");

  var router = require("express").Router();

  // Create a new User
  router.post("/", users.create);
  //login
  router.post("/login", users.login);
  //logout
  router.post("/logout", users.logout);
  // Update with id
  router.put("/:id", users.update);


  app.use('/api/users', router);

};