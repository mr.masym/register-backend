const db = require("../models");
const Users = db.users;
const Loguser = db.loguser;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt');
const saltRounds = 10;

// Create and Save 
exports.create = async (req, res) => {
    const username = req.body.username
    const haveuser = await Users.findOne({where:{username}})
    if (haveuser) {
        res.send({
            message: "Please change user",
            status:false
            
        });
        return;
    }
    const hashpasword = await bcrypt.hash(req.body.password, saltRounds)
    const user = {
        username: req.body.username,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        password: hashpasword,
        email: req.body.email
    };

    // Save in the database
    try {
        const data = await Users.create(user)
        res.send({
            message:"Register complet",
            status:true
        });
    } catch (err) {
        res.status(400).send({
            message:
                err.message || "Some error occurred while creating the Tutorial."
        });
    }
};



// Update by the id in the request
exports.update = (req, res) => {

    const id = req.params.id;

    Users.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Updated successfully."
                });
                logs(id, 'Update', true, 'Succese')
            } else {
                res.send({
                    message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found or req.body is empty!`
                });
                logs(id, 'Update', true, 'Fail not found')
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });


};


// function login
exports.login = async (req, res) => {

    
    const username = req.body.username;
    const password = req.body.password;

    const user = await Users.findOne({ where: { username } })
    if (!user) {
        res.json({
            message: "user not found",
            status: false
        })

    } else {
        const match = await bcrypt.compare(password, user.password)
        if (!match) {
            res.json({
                message: "incorect password",
                status: false
                
            })
            logs(user.id, 'Login', false, 'Fail Password not found')

        }
        else {

            delete user.password;
            res.json({
                status: true,
                data: user
            })
            logs(user.id, 'Login', true, 'Succese')
        }
    }


};
const logs = (userId, action, status, message) => {
    const loguser = {
        userId,
        action,
        status,
        message
    }
    return Loguser.create(loguser)

}
exports.logout = async (req, res) => {

    const username = req.body.username;
    const user = await Users.findOne({ where: { username } })
    try {
        res.send({
            message: 'succese'
        })
        logs(user.id, 'LogOut', true, 'Succese')

    }
    catch (err) {
        res.status(500).send({
            message: 'Logout fail'
        })
        logs(user.id, 'LogOut', true, 'Fail')

    }
}