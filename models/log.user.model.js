
module.exports = (sequelize, Sequelize) => {
    const Loguser = sequelize.define("loguser", {
        
        key: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },

        action: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.BOOLEAN
        },
        message: {
            type: Sequelize.STRING
        }




    })
    return Loguser
}